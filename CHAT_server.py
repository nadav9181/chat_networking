import socket
import threading
from concurrent.futures import thread
from threading import Thread, Timer
from typing import List
from time import strftime, gmtime
import time

server_socket = socket.socket()
server_socket.bind(('10.0.0.29', 8888))

server_socket.listen(2)

clients_To_Connect = 2  # enter number of clients to connect


def accept_client(client_number):
    (client_socket, address) = server_socket.accept()

    return client_socket, address


class Client(object):
    def __init__(self, client_number):
        self.socket, self.address = accept_client(client_number)

    def set_number(self, client_num):
        self.number = client_num

def broadcast_message(client1: Client, clients: List[Client]):
    while True:
        msg1 = client1.socket.recv(1024)
        for client in clients:
            if (client is not client1):  # doesnt send to the client that send the messege
                client.socket.send(bytes('{}: {}'.format(client1.number, msg1), 'utf-8'))


def timer(clients: List[Client]):  # send time every minute
    while True:
        t = 60
        while t > 0:
            time.sleep(1)
            t -= 1
        current_time = strftime('%H:%M:%S', gmtime())
        for client in clients:
            client.socket.send(bytes('the time is: {current_time}'.format(current_time=current_time), 'utf-8'))

def client_num(client: Client):
    msg = 'Enter serial name: '
    number = client.socket.recv(1024)
    client.set_number(number)


def main():
    clients = []
    for client_id in range(clients_To_Connect):
        clients.append(Client(client_id))

    for client in clients:
        client_num(client)
        print("client {client_number} joined the chat with IP: {client_ip}".format(client_number=client.number,
                                                                                   client_ip=client.address))
        client.socket.send(bytes("you are in the chat!".format(client.number), 'utf-8'))

    for client in clients:
        Thread(target=broadcast_message, args=(client, clients)).start()

    Thread(timer(clients)).start()

    while True:
        num = 0

    server_socket.close()


if __name__ == '__main__':
    main()
