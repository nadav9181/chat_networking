import multiprocessing
import socket
import sys
import threading

client_socket = socket.socket()
client_socket.connect(('10.0.0.29', 8888))


def writer():
    while True:
        print('enter text: ')
        msg = input()
        client_socket.send(bytes(msg, 'utf-8'))


def reader():
    while True:
        msg = client_socket.recv(1024)
        print(msg)
        sys.stdout.flush()


def num_client():
    print('enter serial num: ')
    client_num = input()
    return client_num


def main():
    number = num_client()
    client_socket.send(bytes(number, 'utf-8'))
    welcome_message = client_socket.recv(1024)
    print(welcome_message.decode("utf-8"))


    threading.Thread(target=reader).start()
    writer()


if __name__ == '__main__':
    main()
